* assemble/link as raw ROM

	.globl nopars
	.globl common
	.globl _blk_op_lba
	.globl basrom
	.globl voutstr
	.globl voutchr
	.globl vget16
	.globl vcmdlin

	.area cartstart

magic	fcc	'DK'
	bra	start

	* check for shift key -> skip
	* check for D key -> skip SD card
	* check for S key -> skip DWLOAD ?
	* try sdboot, on fail continue
	* - copy sdboot down to $0E00 first
	* try DWLOAD

	* things from original EXTBAS
	* enable PIA0 for 50Hz interrupt and enable interrupts?
	* display sign-on message
	* warm start address? or jump to A0E2-5 to set default one?
	* jump to BASIC A0E2

signon	fcc	"SDBOOT ROM V1"
	fcb	$0D,$00

start
	; basrom is linked with RAM address, but we call the "original" in ROM
	jsr	basrom-ramcode+tocopy		; detect BASIC ROM
	ldx	#signon-1
	jsr	[voutstr]

* check for SHIFT key

chkshift lda   $FF02		; save PIA
         ldb   #$7F
         stb   $FF02
         ldb   $FF00
         sta   $FF02		; restore PIA
         comb
         andb  #$40
         beq   copyrom
         jmp   [vcmdlin]	; return if shift key pressed

* copy other code down to $0E00 (cartridge ROM will unmapped later)

copyrom
	ldx	#tocopy
	ldu	#$0E00		; start of .common in .link file
copyus	ldd	,x++
	std	,u++
	cmpx	#magic+$0E00	; FIXME end of .text
	blo	copyus
	jmp	ramcode

tocopy	equ *

	.area	.common

ramcode
	nop			; for easy verification
	lda	#1
	sta	dumpflag

* check for pressed BREAK key
	lda   $FF02		; save PIA
	ldb   #$FF-$04
	stb   $FF02
	ldb   $FF00
	sta   $FF02		; restore PIA
	comb
	andb  #$40
	beq   defsd
* try SD card from alternate start sector
	ldx   #192
	stx   _blk_op_lba+2
	jsr   common
	bra   gobasic

* try SD card from default sector
defsd	jsr	nopars		; FIXME return here might be broken

gobasic	jmp	[vcmdlin]
	; jmp	$A0E2

