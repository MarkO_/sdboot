* Replace Extended BASIC with SDBOOT loader
* assemble/link as raw ROM

OUTSTR	equ $B99C	; CoCo ROM
CMDLINE	equ $A0D5

	.globl nopars
	.area romstart

magic	fcc	'EX'
	bra	start

	* check for shift key -> skip
	* check for D key -> skip SD card
	* check for S key -> skip DWLOAD ?
	* try sdboot, on fail continue
	* try DWLOAD

	* things from original EXTBAS
	* enable PIA0 for 50Hz interrupt and enable interrupts?
	* display sign-on message
	* warm start address? or jump to A0E2-5 to set default one?
	* jump to BASIC A0E2

signon	fcc	"SDBOOT/DWLOAD EXT ROM"
	fcb	$0D,$00

start
	ldx	#signon-1
	jsr	OUTSTR

* check for SHIFT key

chkshift lda   $FF02		; save PIA
         ldb   #$7F
         stb   $FF02
         ldb   $FF00
         sta   $FF02		; restore PIA
         comb
         andb  #$40
         lbne  CMDLINE		; return if shift key pressed

* copy other code down to $0E00 (BASIC ROM will unmapped later)

	ldx	#tocopy
	ldu	#$0E00		; start of .common in .link file
copyus	ldd	,x++
	std	,u++
	cmpx	#magic+$0E00	; FIXME end of .text
	blo	copyus

	jmp	ramcode

tocopy	equ *

	.area	.common

ramcode
	nop			; for easy verification

* try SD card

	lda	#1
	sta	dumpflag
	jsr	nopars		; FIXME return here might be broken

	jmp	CMDLINE
	jmp	$A0E2

