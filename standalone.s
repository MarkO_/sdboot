;
; SD card loader for Dragon/CoCo with MOOH or Spinx-512 board
;
; Loads a DECB binary from raw sectors on the SD card.
;
; Copyright 2016-2018 Tormod Volden
;

	.globl _Delay
	.globl _dputs
	.globl _main
	.globl _blk_op
	.globl _blk_op_is_read
	.globl _blk_op_CT_BLOCK
	.globl _blk_op_lba
	.globl dumpflag
	.globl nopars
	.globl common
	.globl basrom
	.globl voutchr
	.globl voutstr
	.globl vget16
	.globl vcmdlin

hdrbuf	equ $19f0	; for copying header (use stack instead?)
buffer	equ hdrbuf+5	; for sector from SD card
endbuf	equ buffer+$200	; everything fits below $1C00
tmpsp	equ $3fc	; safe tmp area for BASIC

	.area .data

_blk_op		rmb 2  ; load address
_blk_op_lba	rmb 4
dumpflag	rmb 1  ; zero means dump enabled
mapped		rmb 1  ; zero if BASIC ROM is mapped
tfered		rmb 1  ; received at least one block
sstack		rmb 2  ; saved stack
voutstr		rmb 2
vget16		rmb 2
voutchr		rmb 2
vcmdlin		rmb 2

	.area .main

_main	bra start		; fixed entry point

_blk_op_is_read	 fcb 1
_blk_op_CT_BLOCK fcb 1

basrom	lda $80FD		; test byte in BASIC ROM
	cmpa #$49		; Dragon 32 value
	beq dgnrom
	ldx #$B99C		; CoCo BASIC
	stx voutstr
	ldx #$B73D
	stx vget16
	ldx #$A282
	stx voutchr
	ldx #$A0D5
	stx vcmdlin
	rts
dgnrom	ldx #$90E5
	stx voutstr
	ldx #$8E83
	stx vget16
	ldx #$B54A
	stx voutchr
	ldx #$B43C
	stx vcmdlin
	rts

* used in SD card initialization, delay X millisecond
_Delay
loopx	lda #$B0
loopa	deca
	bne loopa
	leax -1,x
	bne loopx
	rts

* this may be called from bulk transfer code, with ROM mapped out
_dputs
	leax -1,x
	tst mapped
	beq nomap
	ldd $FFA4		; save MOOH mapping
	pshs u,d
	ldd #$3F3F		; expose BASIC ROM at $8000-$BFFF
	stb $FFA5		; set-reset of Spinx-512 register
	sta $FFA4
	sta $FFBE		; expose BASIC ROM on Spinx-512
	jsr [voutstr]
	puls u,d
	stb $FFA5		; restore MOOH mapping
	sta $FFA4
	clr $FFBF		; enable Spinx-512 again
	rts
nomap	pshs u
	jsr [voutstr]
	puls u,pc

* entry point from command line
start
	bsr basrom		; detect BASIC ROM
	lda #1
	sta dumpflag		; default non-zero = dump not enabled
* parse command options before any hw access
	jsr <$a5		; peek at next char
	beq nopars
	cmpa #',		; used with e.g. EXEC&H103B,N16
	bne nocm
	jsr <$9f		; eat and ignore comma
	jsr <$a5		; peek at next char
nocm	suba #'N		; only load and dump first bytes
	sta dumpflag		; zero if dump mode
	bne nodump
	jsr <$9f		; eat N
nodump	jsr <$a5		; more args?
	beq nopars
	jsr [vget16]		; LSW of LBA
	stx _blk_op_lba+2
	bra common

* entry point from boot ROM, must have set up dumpflag
nopars
	ldx #64			; default LBA LSW
	stx _blk_op_lba+2
common	ldx #0			; default LBA MSW
	stx _blk_op_lba
	bsr hwinit		; may rts us
	bra loaddecb

; might be used at a later point
;getmbr
;	ldx #0			; read LBA 0
;	stx _blk_op_lba
;	stx _blk_op_lba+2
;	ldx #buffer
;	stx _blk_op
;	jsr _devsd_transfer_sector
;	ldx buffer+$1FE
;	cmpx #$55AA		; MBR magic
;	bne defsect
;	lda buffer+$1C2
;	cmpa #$EE		; protected MBR

hwinit
	clr mapped		; before first dputs
      IFNDEF NOSDINIT
* check for SPI interface
	jsr _spi_setup
	bne spiok
	ldx #spi65_fail
hwfail	bsr _dputs
	leas 2,s		; return to caller's caller
	rts
spiok
* detect and initialize SD card
	jsr _sd_spi_init
	cmpx #0			; CT_NONE
	bne cardok
	ldx #card_fail
	bra hwfail
cardok	ldx #card_ok
	lbra _dputs
      ELSE
	rts
      ENDC ; NOSDINIT

loaddecb
* point of uncertain return
	orcc #$50		; in case vectors and handlers will be loaded
* we are running from internal, lower RAM, disable MMU for now
	clra
	sta $FF90
* configure MMU mapping: Pages 0-3 into $8000-$FEFF
	lda #3			; Done backwards to not upset spinx-512
	sta $FFA7		; which uses these addresses for
	deca			; segmented activation in $4000-$7FFF.
	sta $FFA6
	deca
	sta $FFA5
	deca
	sta $FFA4
	sta tfered		; clear flag
	ldx #tmpsp
	exg x,s
	stx sstack		; save caller's SP

	ldx #buffer		; load address (buffer)
	stx _blk_op
	ldu #endbuf		; start with empty buffer
* read a DECB segment
nextseg	ldy #5
	ldx #hdrbuf	; copy DECB header here
	bsr copybuf	; get header, moves x ahead
	lda -5,x	; first byte is zero?
	bne eodecb
ml	ldy -4,x	; normal data segment, length
	lbeq errdecb	; segment length must be > 0
	ldx -2,x	; load address
	bsr copybuf	; get segment
	bra  nextseg
eodecb	inca
	lbne errdecb	; must be $ff otherwise
	lda tfered	; at least one data segment
	lbeq errdecb
	ldu sstack	; restore original SP
	tfr u,s
	ldx -2,x	; exec address
	stx <$9d	; BASIC EXEC address
	jmp ,x

copybuf
copyl	cmpu #endbuf
	bne copym	; still more bytes in buffer
	cmpy #512
	bge direct
retdir	pshs x,y	; in use in copym
	jsr _devsd_transfer_sector
	puls x,y
	cmpb #1
	lbne fail_tfer
	stb tfered	; set flag
	lda dumpflag
	beq dumphead	; just read one sector and leave
	ldu _blk_op_lba+2
	leau 1,u	; update LBA counter
	stu _blk_op_lba+2
	lda #'*		; progress bar
	jsr [voutchr]
	ldu #buffer
	ldb #64		; for MMU enable
copym	lda ,u+
	clr $FFBF
	stb $FF90
	sta ,x+
	clr $FFBE
	clr $FF90
	leay -1,y
	bne copyl
	rts

* read full sectors straight into destination X
direct
	lda #64		; for MMU enable
	stx _blk_op	; destination
	sta mapped	; for dputs
	clr $FFBF
	sta $FF90
	pshs x,y
	jsr _devsd_transfer_sector
	puls x,y
	clr $FFBE
	clr $FF90
	clr mapped
	cmpb #1
	bne fail_tfer
	ldu _blk_op_lba+2
	leau 1,u	; update LBA counter
	stu _blk_op_lba+2
	lda #'+		; progress bar
	jsr [voutchr]
	leax 512,x
	leay -512,y
	cmpy #512
	bge direct
	ldu #buffer	; via buffer again
	stu _blk_op
	ldu #endbuf
	cmpy #0
	lbne retdir	; finish DECB segment
	rts

dumphead
	ldx #tfer_ok
abort
	ldu sstack	; restore original SP
	tfr u,s		; let's hope old stack is intact
	andcc #~$50	; good luck
	lbra _dputs	; write message and quit
fail_tfer
	ldx #tfer_fail
	bra abort
errdecb
	ldx #decb_fail
	bra abort

spi65_fail fcc /NO 65SPI/
	fcb $0D,$00
card_ok fcc /CARD DETECTED/
	fcb $0D,$00
card_fail fcc /NO CARD/
	fcb $0D,$00
tfer_ok fcc /SUCCESSFUL TRANSFER/
	fcb $0D,$00
tfer_fail fcc /TRANSFER FAILED/
	fcb $0D,$00
decb_fail fcc /INVALID DECB/
	fcb $0D,$00
