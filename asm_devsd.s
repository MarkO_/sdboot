* SPI mode SD card transfer routine
* Rewrite of devsd.c in 6809 assembly
* Copyright 2017 Tormod Volden

	.globl _devsd_transfer_sector


CMD17	equ $40+17			; READ_SINGLE_BLOCK
CMD24	equ $40+24			; WRITE_BLOCK

	.area .text

* returns status in B
* clobbers A,B,X,Y
_devsd_transfer_sector
	* TODO support bkl_op_CT_BLOCK = 0 (byte addressed cards)
	ldx #_blk_op_lba
	lda #8				; attempts
	pshs a
attmpt
	tst _blk_op_is_read
	bne isread
	ldb #CMD24
	bra sndcmd
isread	ldb #CMD17
sndcmd	jsr _sd_send_command
	tstb
	bne dontr

	tst _blk_op_is_read
	beq iswrite
	clrb
	jsr _sd_spi_wait
	subb #$FE
	bne dontr
	jsr _sd_spi_receive_sector
	clrb
	bra dontr
iswrite ldb #1
	jsr _sd_spi_wait
	subb #$FF
	bne dontr
	ldb #$FE
	jsr _sd_spi_transmit_byte
	jsr _sd_spi_transmit_sector
	ldb #$FF			; dummy CRC
	jsr _sd_spi_transmit_byte
	jsr _sd_spi_transmit_byte
	clrb
	jsr _sd_spi_wait
	andb #$1F
	subb #$05
dontr	pshs b				; 0 = success
	jsr _sd_spi_release
	puls b
	tstb
	beq retsucc
	* TODO print retry message
	dec ,s
	bne attmpt
	clrb				; return failure on timeout
reta	puls a,pc			; restore stack
retsucc ldb #1
	bra reta


* clobbers A,B
_sd_spi_release
	jsr _sd_spi_raise_cs
	jmp _sd_spi_receive_byte


* want_ff in B, returns status in B
* clobbers A,Y
* TODO eventually more efficient to have two functions

_sd_spi_wait
	ldy #500
	tstb
	beq noff
waitl	jsr _sd_spi_receive_byte	; clobbers A,B
	cmpb #$FF
	beq don1
	leay -1,y
	bne waitl
	* TODO print timeout error message
don1	rts

noff	jsr _sd_spi_receive_byte	; clobbers A,B
	cmpb #$FF
	bne don1
	leay -1,y
	bne noff
	bra don1	; TODO jump to timeout error message


* cmd in B, pointer to arg in X, returns status in B
* clobbers A,Y

_sd_send_command
	* TODO deal with ACMD

	pshs b
	jsr _sd_spi_release		; clobbers A,B
	jsr _sd_spi_lower_cs		; clobbers A
	ldb #1
	jsr _sd_spi_wait		; clobbers A,B,Y
	cmpb #$FF
	beq wok
	ldb #$FF
	puls a,pc			; restore stack
wok
	ldb ,s
	jsr _sd_spi_transmit_byte	; send command index, clobbers A

	ldb ,x
	jsr _sd_spi_transmit_byte	; arg[31..24], clobbers A
	ldb 1,x
	jsr _sd_spi_transmit_byte	; arg[23..16]
	ldb 2,x
	jsr _sd_spi_transmit_byte	; arg[15..8]
	ldb 3,x
	jsr _sd_spi_transmit_byte	; arg[7..0]

	ldb #1
	* ; TODO check for CMD0 or CMD8
	jsr _sd_spi_transmit_byte	; CRC

	* ; TODO check for CMD12

	ldy #20
waitresp
	jsr _sd_spi_receive_byte
	tstb
	bpl valresp
	leay -1,y
	bne waitresp
valresp puls a,pc			; restore stack

	end
